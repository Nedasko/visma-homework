﻿using System;
using System.Linq;
using ContactManager.Properties;
using System.Collections.Specialized;

namespace Homework
{
    public class Contacts
    {
        public String Name { get; set; }
        public String LastName { get; set; }
        public String PhoneNumber { get; set; }
        public String Adress { get; set; }
    }
    class Program
    {
        static void AddContact()
        {
            var contact = new Contacts();     

            bool proceed = true;
            while (proceed)
            { 
                Console.WriteLine("Enter name");
                contact.Name = Console.ReadLine();
                Settings.Default.Name.Add(contact.Name);


                Console.WriteLine("Enter last name");
                contact.LastName = Console.ReadLine();
                Settings.Default.LastName.Add(contact.LastName);

                Console.WriteLine("Enter Phone number");
                contact.PhoneNumber = Console.ReadLine();
                Settings.Default.PhoneNumber.Add(contact.PhoneNumber);

                Console.WriteLine("Enter Adress");
                contact.Adress = Console.ReadLine();
                Settings.Default.Adress.Add(contact.Adress);

                if (String.IsNullOrWhiteSpace(contact.Name) || String.IsNullOrWhiteSpace(contact.LastName) || String.IsNullOrWhiteSpace(contact.PhoneNumber))
                {
                    Console.WriteLine("You can't leave name, last name or phone number fields empty");
                    break;
                }

                bool trigger = false;
                for (int i = 0; i < Settings.Default.PhoneNumber.Count; i++)
                {
                    if (Settings.Default.PhoneNumber.Count >= 1)
                    {
                        for (int j = 1; j < Settings.Default.PhoneNumber.Count; j++)
                        {
                            if (Settings.Default.PhoneNumber[i] == Settings.Default.PhoneNumber[j])
                            {
                                Settings.Default.Name.RemoveAt(j);
                                Settings.Default.LastName.RemoveAt(j);
                                Settings.Default.PhoneNumber.RemoveAt(j);
                                Settings.Default.Adress.RemoveAt(j);
                                
                                Console.WriteLine("Phone numbers cannot be repeated");
                                trigger = true;
                            }
                        }
                    }
                    break;
                }
                    if (!trigger)
                    Settings.Default.Save();

                Console.WriteLine("\n");
                Console.WriteLine("Do you want to proceed adding contacts [Yes][No]");

                string choice = Console.ReadLine();
                if (choice.ToLower() == "yes")
                    proceed = true;
                else break;
            }
        }

        static void UpdateContact()
        {
            var contact = new Contacts();

            Console.WriteLine("Which contact you want to update [Index]");

            for (int i = 0; i < Settings.Default.Name.Count; i++)
            {
                Console.WriteLine("Press {0} to update {1} {2} contact", i, Settings.Default.Name[i], Settings.Default.LastName[i]);
            }

            var choice = Console.ReadLine();
            try
            {
                if (choice.All(char.IsDigit) && Settings.Default.Name.Count > 0)
                {
                    if (int.Parse(choice) <= Settings.Default.Name.Count)
                    {
                        Console.WriteLine("Enter new name");
                        contact.Name = Console.ReadLine();
                        if (!String.IsNullOrWhiteSpace(contact.Name))
                        Settings.Default.Name[int.Parse(choice)] = contact.Name;

                        Console.WriteLine("Enter new last name");
                        contact.LastName = Console.ReadLine();
                        if (!String.IsNullOrWhiteSpace(contact.LastName))
                        Settings.Default.LastName[int.Parse(choice)] = contact.LastName;

                        Console.WriteLine("Enter new Phone number");
                        contact.PhoneNumber = Console.ReadLine();
                        if (!String.IsNullOrWhiteSpace(contact.PhoneNumber))
                        Settings.Default.PhoneNumber[int.Parse(choice)] = contact.PhoneNumber;

                        Console.WriteLine("Enter new Adress");
                        contact.Adress = Console.ReadLine();
                        Settings.Default.Adress[int.Parse(choice)] = contact.Adress;

                        Settings.Default.Save();
                        
                    }
                    else throw new ArgumentException();
                }
                else throw new ArgumentException();
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        static void DeleteContact()
        {
            Console.WriteLine("Which contact you want to delete [Index]");
            
            for(int i=0;i< Settings.Default.Name.Count;i++)
            {
                Console.WriteLine("Press {0} to delete {1} {2} contact", i, Settings.Default.Name[i], Settings.Default.LastName[i]);
            }

            var choice = Console.ReadLine();
            try
            {
                if (choice.All(char.IsDigit))
                {
                    if (int.Parse(choice) <= Settings.Default.Name.Count)
                    {
                        Settings.Default.Name.RemoveAt(int.Parse(choice));
                        Settings.Default.LastName.RemoveAt(int.Parse(choice));
                        Settings.Default.PhoneNumber.RemoveAt(int.Parse(choice));
                        Settings.Default.Adress.RemoveAt(int.Parse(choice));

                        Settings.Default.Save();
                    }
                    else throw new ArgumentException();
                }
                else throw new ArgumentException();
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.Message);
            }

        }
        static void ViewContacts()
        {
            for (int i = 0; i < Settings.Default.Name.Count; i++)
            {
                Console.WriteLine("\n");
                Console.WriteLine("Name is " + Settings.Default.Name[i].ToString());
                Console.WriteLine("Last name is " + Settings.Default.LastName[i].ToString());
                Console.WriteLine("Phone number is " + Settings.Default.PhoneNumber[i].ToString());
                Console.WriteLine("Adress is " + Settings.Default.Adress[i].ToString());
                Console.WriteLine("\n");
            }
        }
        static void Main(string[] args)
        {
            if (StringCollection.Equals(Settings.Default.Name,null))
            {
                Settings.Default.Name = new StringCollection();
                Settings.Default.LastName = new StringCollection();
                Settings.Default.PhoneNumber = new StringCollection();
                Settings.Default.Adress = new StringCollection();
            }               

            while (true)
            {
                Console.WriteLine("\n");
                Console.WriteLine("MENU");
                Console.WriteLine("--------------------------");
                Console.WriteLine("Press 1 to: Add contact");
                Console.WriteLine("Press 2 to: Update contact");
                Console.WriteLine("Press 3 to: Delete contact");
                Console.WriteLine("Press 4 to: View contacts");
                Console.WriteLine("--------------------------");
                Console.WriteLine("\n");

                string n = Console.ReadLine();
                try
                {
                    if (n.All(char.IsDigit))
                    {
                        if (int.Parse(n) != 1 && int.Parse(n) != 2 && int.Parse(n) != 3 && int.Parse(n) != 4)
                        {
                            throw new ArgumentException();
                        }
                    }
                    else throw new ArgumentException();
                }
                catch (ArgumentException e)
                {
                    Console.WriteLine(e.Message);
                }

                if (int.Parse(n) == 1)
                    AddContact();
                if (int.Parse(n) == 2)
                    UpdateContact();
                if (int.Parse(n) == 3)
                    DeleteContact();
                if (int.Parse(n) == 4)
                    ViewContacts();
            }
        }
    }

}